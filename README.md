# Prefab Scripting - Releases

## Prefab Script Release - 1.0
### 2D 
+ 2D Character
+ 2D PlayerShooter
+ 2D Destroyer
+ Object Follower
+ Path Follower
+ Mouse Follower
+ 2D Look At
+ Moving Platformer
+ Set Movement

### 3D
+ 3D Character
+ 3D Character Shooter
+ 3D Destroyer
+ 3D Look At
+ Mouse Drag
+ Object Follower
+ 3D Platformer
+ Set Movement 3D
+ Set Rotation

### UI
+ Dialog System
+ Game Menu
+ Main Menu
+ UI Destroyer
+ UI Creator
+ Pop Up
+ Scene Transition 
+ Vignette

## Prefab Script Release - 1.5

+ New Youtube video tutorials
+ Look At Script in Platformer Tool is now easier to understand
+ New "What's new?" option to see the new changes made for the asset
+ New Title for the asset. (it not a long sentance but just the name of the asset, which is prefab script)

## Prefab Script Release - 2.0
+ New trailer for the asset
+ New tool - 2d spawner
+ New tool - 3d spawner
+ Change in the name of the title of the asset
+ Some minor bugs fixed
